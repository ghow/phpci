<?php 

class Exchange extends Backend_Controller 
{
	public function __construct()
	{
		parent::__construct("backend");			

		$this->per_page = 100;
	}

	public function modify($id="", $skip_validation = FALSE, $redirect_root = TRUE)
	{
		if($this->input->post())
		{
			if(!$this->input->post("status"))
				$_POST["status"] = 0;

			$_POST["time_buy"] = strtotime($this->input->post("time_buy"));
			$_POST["time_sell"] = strtotime($this->input->post("time_sell"));
		}

		parent::modify($id,$skip_validation,$redirect_root);	
	}

	public function index($page = 0)
	{
		$where["status"] = 1;
		$exchanges = $this->model->get_many_by($where);
		$count = 0;

		foreach ($exchanges as $key => $value) 
			if($value->amount_sell - $value->amount_buy > 0) 
				$count += $value->amount_sell - $value->amount_buy;

		$this->data["count"] = $count;

		parent::index($page);
	}

	public function Rate()
	{
   		$this->load->library('simple_html_dom');

   		$rate["aud"] = $this->getRate("aud");
   		$rate["gbp"] = $this->getRate("gbp");
   		$rate["jpy"] = $this->getRate("jpy");
   		$rate["usd"] = $this->getRate("usd");
   		$rate["cny"] = $this->getRate("cny");
		
		$this->data["rate"] = $rate;
		$this->load->view("backend/Exchange/Rate", $this->data);
	}

	private function getRate($currency)
	{
		$result = array();
		if($this->input->get("from"))
			$get = "?from=" . $this->input->get("from");
		else
			$get = "";

		header("Content-Type:text/html; charset=utf-8");
		$uri = 'http://www.bestxrate.com/bankrate/twesun/' . $currency . '/buytt.html' . $get; 
		$html = file_get_html($uri); 

		$table = $html->find("table");  
		$table = $table[1]->find("tbody tr td");

		switch ($currency) {
			case 'aud':
				$deviation = 0.2;
				break;
			case 'gbp':
				$deviation = 0.4;
				break;
			case 'jpy':
				$deviation = 0.004;
				break;
			case 'usd':
				$deviation = 0.1;
				break;
			case 'cny':
				$deviation = 0.05;
				break;
			
			default:
				$deviation = 0;
				break;
		}
		$result = $this->AnalysisRate($table, $deviation);

		return $result;
	}

	private function AnalysisRate($table, $deviation)
	{
		$i = 1;
		$result = array();
		$exchanges = array();

		for ($i=0; $i < count($table); $i += 5) 
		{ 
			$exchanges["date"] = $table[$i]->plaintext;
			$exchanges["buy"] = (float) $table[$i + 2]->plaintext;
			$exchanges["sell"] = (float) $table[$i + 2]->plaintext + $deviation;
			$result[] = $exchanges;
		}
		return $result;
	}
}