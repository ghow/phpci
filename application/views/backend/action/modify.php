<!DOCTYPE html>
<html lang="en">
<? $this->load->view("backend/partials/meta"); ?>

<body>
  <!-- Start Page Loading -->
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START TOP -->
  <? $this->load->view("backend/partials/top"); ?>
  <!-- END TOP -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEBAR -->
  <? $this->load->view("backend/partials/sidebar"); ?>
  <!-- END SIDEBAR -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START CONTENT -->
  <div class="content">
    <!-- Start Page Header -->
    <div class="page-header">
      <h1 class="title">系統帳號管理</h1>
      <ol class="breadcrumb">
        <li><a href="<?=site_url("backend/cms_user")?>">系統管理</a></li>
        <li><a href="<?=site_url("backend/cms_user")?>">系統帳號管理</a></li>
        <li class="active">編輯</li>
      </ol>
      <!-- Start Page Header Right Div -->
      <!-- End Page Header Right Div -->
    </div>
    <!-- End Page Header -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTAINER -->
    <div class="container-padding">
      <? if($err_msg):?>
      <? foreach($err_msg as $k=>$v):?>
        <div class="kode-alert kode-alert-icon kode-alert-click alert6-light">
          <i class="fa fa-warning"></i>
          <a href="#" class="closed">&times;</a>
          <?=$v?>
        </div>
      <? endforeach;?>
      <? endif;?>
      <!-- Start Row -->
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-title">
            </div>
            <div class="panel-body">
              <form class="form-horizontal" method="post">
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label"><span style="color:red"> * </span>帳號</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="account" value="<?=$item?$item->account:""?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label"><span style="color:red"> * </span>密碼</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="password_admin" value="<?=$item?$item->password_admin:""?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label">姓名</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="<?=$item?$item->name:""?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label form-label">E-mail</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="email" value="<?=$item?$item->email:""?>">
                  </div>
                </div>
                <p class="col-sm-offset-2"><span style="color:red"> 註：如需加入多個E-Mail請以,隔開。 </span></p>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default submit">送出</button>
                    <button class="btn btn-light" type="button" onclick="location.href='<?=site_url($site_root)?>'">返回</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row -->
    </div>
    <!-- END CONTAINER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- Start Footer -->
    <? $this->load->view("backend/partials/footer");?>
    <!-- End Footer -->
  </div>
  <!-- End Content -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEPANEL -->
  <!-- END SIDEPANEL -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
<? $this->load->view("backend/partials/script");?>
<script>
</script>
</body>
</html>