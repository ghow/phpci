/* ======================================================================
SCALED Currency AUD
====================================================================== */
var data, graph, i, max, min, point, random, scales, series, _k, _l, _len, _len1, _len2, _ref, rate;

data = [[], []];
rate = JSON.parse($("#aud").val());

for (i = 0; i < rate.length; i++) {
  // 傳入時間數字及數值
  var dt = new Date(rate[i]["date"]);

  data[0][i] = {'x': dt.getTime() / 1000 + 8 * 60 * 60 , 'y': parseFloat(rate[i]["buy"])};
  data[1][i] = {'x': dt.getTime() / 1000 + 8 * 60 * 60 , 'y': parseFloat(rate[i]["sell"])};
}

scales = [];

for (_k = 0, _len1 = data.length; _k < _len1; _k++) {
  series = data[_k];
  min = Number.MAX_VALUE;
  max = Number.MIN_VALUE;

  for (_l = 0, _len2 = series.length; _l < _len2; _l++) {
    point = series[_l];
    min = Math.min(min, point.y);
    max = Math.max(max, point.y);
  }
  if (_k === 0) {
    scales.push(d3.scale.linear().domain([min, max]).nice());
  } else {
    scales.push(d3.scale.linear().domain([min, max]).nice());
  }
}

graph = new Rickshaw.Graph({
  element: document.getElementById("rickshaw-aud"),
  renderer: 'line',
  series: [
    {
      color: '#f4792f',
      data: data[0],
      name: '銀行買入',
      scale: scales[0]
    }, {
      color: '#428bca',
      data: data[1],
      name: '銀行賣出',
      scale: scales[0]
    }
  ]
});

new Rickshaw.Graph.Axis.Y.Scaled({
  element: document.getElementById('axis0'),
  graph: graph,
  orientation: 'right',
  scale: scales[0],
  // tickFormat: Rickshaw.Fixtures.Number.formatKMBT
});

new Rickshaw.Graph.Axis.Y.Scaled({
  element: document.getElementById('axis1'),
  graph: graph,
  orientation: 'right',
  scale: scales[0],
  // tickFormat: Rickshaw.Fixtures.Number.formatKMBT
});

new Rickshaw.Graph.Axis.Time({
  graph: graph
});


Date.prototype.format = function(format)
{ 
  var o = { 
    "M+" : this.getMonth()+1, //month 
    "d+" : this.getDate(),    //day 
    "h+" : this.getHours(),   //hour 
    "m+" : this.getMinutes(), //minute 
    "s+" : this.getSeconds(), //second 
    "q+" : Math.floor((this.getMonth()+3)/3),  //quarter 
    "S" : this.getMilliseconds() //millisecond 
  } 
  if(/(y+)/.test(format)) format=format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
  for(var k in o)if(new RegExp("("+ k +")").test(format)) 
    format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length)); 
  return format; 
} 

new Rickshaw.Graph.HoverDetail({
  graph: graph,

  formatter: function(series, x, y) {
    var date = '<span class="date">' + new Date( x * 1000 ).format('yyyy/M/d') + '</span>';
    var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
    var content = date + ' ' + swatch + series.name + ": " + y;
    return content;
  }
});

graph.render();

/* ======================================================================
SCALED Currency GBP
====================================================================== */
var data, graph, i, max, min, point, random, scales, series, _k, _l, _len, _len1, _len2, _ref, rate;

data = [[], []];
rate = JSON.parse($("#gbp").val());

for (i = 0; i < rate.length; i++) {
  // 傳入時間數字及數值
  var dt = new Date(rate[i]["date"]);

  data[0][i] = {'x': dt.getTime() / 1000 + 8 * 60 * 60 , 'y': parseFloat(rate[i]["buy"])};
  data[1][i] = {'x': dt.getTime() / 1000 + 8 * 60 * 60 , 'y': parseFloat(rate[i]["sell"])};
}

scales = [];

for (_k = 0, _len1 = data.length; _k < _len1; _k++) {
  series = data[_k];
  min = Number.MAX_VALUE;
  max = Number.MIN_VALUE;

  for (_l = 0, _len2 = series.length; _l < _len2; _l++) {
    point = series[_l];
    min = Math.min(min, point.y);
    max = Math.max(max, point.y);
  }
  if (_k === 0) {
    scales.push(d3.scale.linear().domain([min, max]).nice());
  } else {
    scales.push(d3.scale.linear().domain([min, max]).nice());
  }
}

graph = new Rickshaw.Graph({
  element: document.getElementById("rickshaw-gbp"),
  renderer: 'line',
  series: [
    {
      color: '#f4792f',
      data: data[0],
      name: '銀行買入',
      scale: scales[0]
    }, {
      color: '#428bca',
      data: data[1],
      name: '銀行賣出',
      scale: scales[0]
    }
  ]
});

new Rickshaw.Graph.Axis.Y.Scaled({
  element: document.getElementById('axis0'),
  graph: graph,
  orientation: 'right',
  scale: scales[0],
  // tickFormat: Rickshaw.Fixtures.Number.formatKMBT
});

new Rickshaw.Graph.Axis.Y.Scaled({
  element: document.getElementById('axis1'),
  graph: graph,
  orientation: 'right',
  scale: scales[0],
  // tickFormat: Rickshaw.Fixtures.Number.formatKMBT
});

new Rickshaw.Graph.Axis.Time({
  graph: graph
});


Date.prototype.format = function(format)
{ 
  var o = { 
    "M+" : this.getMonth()+1, //month 
    "d+" : this.getDate(),    //day 
    "h+" : this.getHours(),   //hour 
    "m+" : this.getMinutes(), //minute 
    "s+" : this.getSeconds(), //second 
    "q+" : Math.floor((this.getMonth()+3)/3),  //quarter 
    "S" : this.getMilliseconds() //millisecond 
  } 
  if(/(y+)/.test(format)) format=format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
  for(var k in o)if(new RegExp("("+ k +")").test(format)) 
    format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length)); 
  return format; 
} 

new Rickshaw.Graph.HoverDetail({
  graph: graph,

  formatter: function(series, x, y) {
    var date = '<span class="date">' + new Date( x * 1000 ).format('yyyy/M/d') + '</span>';
    var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
    var content = date + ' ' + swatch + series.name + ": " + y;
    return content;
  }
});

graph.render();

/* ======================================================================
SCALED Currency JPY
====================================================================== */
var data, graph, i, max, min, point, random, scales, series, _k, _l, _len, _len1, _len2, _ref, rate;

data = [[], []];
rate = JSON.parse($("#jpy").val());

for (i = 0; i < rate.length; i++) {
  // 傳入時間數字及數值
  var dt = new Date(rate[i]["date"]);

  data[0][i] = {'x': dt.getTime() / 1000 + 8 * 60 * 60 , 'y': parseFloat(rate[i]["buy"])};
  data[1][i] = {'x': dt.getTime() / 1000 + 8 * 60 * 60 , 'y': parseFloat(rate[i]["sell"])};
}

scales = [];

for (_k = 0, _len1 = data.length; _k < _len1; _k++) {
  series = data[_k];
  min = Number.MAX_VALUE;
  max = Number.MIN_VALUE;

  for (_l = 0, _len2 = series.length; _l < _len2; _l++) {
    point = series[_l];
    min = Math.min(min, point.y);
    max = Math.max(max, point.y);
  }
  if (_k === 0) {
    scales.push(d3.scale.linear().domain([min, max]).nice());
  } else {
    scales.push(d3.scale.linear().domain([min, max]).nice());
  }
}

graph = new Rickshaw.Graph({
  element: document.getElementById("rickshaw-jpy"),
  renderer: 'line',
  series: [
    {
      color: '#f4792f',
      data: data[0],
      name: '銀行買入',
      scale: scales[0]
    }, {
      color: '#428bca',
      data: data[1],
      name: '銀行賣出',
      scale: scales[0]
    }
  ]
});

new Rickshaw.Graph.Axis.Y.Scaled({
  element: document.getElementById('axis0'),
  graph: graph,
  orientation: 'right',
  scale: scales[0],
  // tickFormat: Rickshaw.Fixtures.Number.formatKMBT
});

new Rickshaw.Graph.Axis.Y.Scaled({
  element: document.getElementById('axis1'),
  graph: graph,
  orientation: 'right',
  scale: scales[0],
  // tickFormat: Rickshaw.Fixtures.Number.formatKMBT
});

new Rickshaw.Graph.Axis.Time({
  graph: graph
});


Date.prototype.format = function(format)
{ 
  var o = { 
    "M+" : this.getMonth()+1, //month 
    "d+" : this.getDate(),    //day 
    "h+" : this.getHours(),   //hour 
    "m+" : this.getMinutes(), //minute 
    "s+" : this.getSeconds(), //second 
    "q+" : Math.floor((this.getMonth()+3)/3),  //quarter 
    "S" : this.getMilliseconds() //millisecond 
  } 
  if(/(y+)/.test(format)) format=format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
  for(var k in o)if(new RegExp("("+ k +")").test(format)) 
    format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length)); 
  return format; 
} 

new Rickshaw.Graph.HoverDetail({
  graph: graph,

  formatter: function(series, x, y) {
    var date = '<span class="date">' + new Date( x * 1000 ).format('yyyy/M/d') + '</span>';
    var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
    var content = date + ' ' + swatch + series.name + ": " + y;
    return content;
  }
});

graph.render();

/* ======================================================================
SCALED Currency USD
====================================================================== */
var data, graph, i, max, min, point, random, scales, series, _k, _l, _len, _len1, _len2, _ref, rate;

data = [[], []];
rate = JSON.parse($("#usd").val());

for (i = 0; i < rate.length; i++) {
  // 傳入時間數字及數值
  var dt = new Date(rate[i]["date"]);

  data[0][i] = {'x': dt.getTime() / 1000 + 8 * 60 * 60 , 'y': parseFloat(rate[i]["buy"])};
  data[1][i] = {'x': dt.getTime() / 1000 + 8 * 60 * 60 , 'y': parseFloat(rate[i]["sell"])};
}

scales = [];

for (_k = 0, _len1 = data.length; _k < _len1; _k++) {
  series = data[_k];
  min = Number.MAX_VALUE;
  max = Number.MIN_VALUE;

  for (_l = 0, _len2 = series.length; _l < _len2; _l++) {
    point = series[_l];
    min = Math.min(min, point.y);
    max = Math.max(max, point.y);
  }
  if (_k === 0) {
    scales.push(d3.scale.linear().domain([min, max]).nice());
  } else {
    scales.push(d3.scale.linear().domain([min, max]).nice());
  }
}

graph = new Rickshaw.Graph({
  element: document.getElementById("rickshaw-usd"),
  renderer: 'line',
  series: [
    {
      color: '#f4792f',
      data: data[0],
      name: '銀行買入',
      scale: scales[0]
    }, {
      color: '#428bca',
      data: data[1],
      name: '銀行賣出',
      scale: scales[0]
    }
  ]
});

new Rickshaw.Graph.Axis.Y.Scaled({
  element: document.getElementById('axis0'),
  graph: graph,
  orientation: 'right',
  scale: scales[0],
  // tickFormat: Rickshaw.Fixtures.Number.formatKMBT
});

new Rickshaw.Graph.Axis.Y.Scaled({
  element: document.getElementById('axis1'),
  graph: graph,
  orientation: 'right',
  scale: scales[0],
  // tickFormat: Rickshaw.Fixtures.Number.formatKMBT
});

new Rickshaw.Graph.Axis.Time({
  graph: graph
});


Date.prototype.format = function(format)
{ 
  var o = { 
    "M+" : this.getMonth()+1, //month 
    "d+" : this.getDate(),    //day 
    "h+" : this.getHours(),   //hour 
    "m+" : this.getMinutes(), //minute 
    "s+" : this.getSeconds(), //second 
    "q+" : Math.floor((this.getMonth()+3)/3),  //quarter 
    "S" : this.getMilliseconds() //millisecond 
  } 
  if(/(y+)/.test(format)) format=format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
  for(var k in o)if(new RegExp("("+ k +")").test(format)) 
    format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length)); 
  return format; 
} 

new Rickshaw.Graph.HoverDetail({
  graph: graph,

  formatter: function(series, x, y) {
    var date = '<span class="date">' + new Date( x * 1000 ).format('yyyy/M/d') + '</span>';
    var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
    var content = date + ' ' + swatch + series.name + ": " + y;
    return content;
  }
});

graph.render();

/* ======================================================================
SCALED Currency CNY
====================================================================== */
var data, graph, i, max, min, point, random, scales, series, _k, _l, _len, _len1, _len2, _ref, rate;

data = [[], []];
rate = JSON.parse($("#cny").val());

for (i = 0; i < rate.length; i++) {
  // 傳入時間數字及數值
  var dt = new Date(rate[i]["date"]);

  data[0][i] = {'x': dt.getTime() / 1000 + 8 * 60 * 60 , 'y': parseFloat(rate[i]["buy"])};
  data[1][i] = {'x': dt.getTime() / 1000 + 8 * 60 * 60 , 'y': parseFloat(rate[i]["sell"])};
}

scales = [];

for (_k = 0, _len1 = data.length; _k < _len1; _k++) {
  series = data[_k];
  min = Number.MAX_VALUE;
  max = Number.MIN_VALUE;

  for (_l = 0, _len2 = series.length; _l < _len2; _l++) {
    point = series[_l];
    min = Math.min(min, point.y);
    max = Math.max(max, point.y);
  }
  if (_k === 0) {
    scales.push(d3.scale.linear().domain([min, max]).nice());
  } else {
    scales.push(d3.scale.linear().domain([min, max]).nice());
  }
}

graph = new Rickshaw.Graph({
  element: document.getElementById("rickshaw-cny"),
  renderer: 'line',
  series: [
    {
      color: '#f4792f',
      data: data[0],
      name: '銀行買入',
      scale: scales[0]
    }, {
      color: '#428bca',
      data: data[1],
      name: '銀行賣出',
      scale: scales[0]
    }
  ]
});

new Rickshaw.Graph.Axis.Y.Scaled({
  element: document.getElementById('axis0'),
  graph: graph,
  orientation: 'right',
  scale: scales[0],
  // tickFormat: Rickshaw.Fixtures.Number.formatKMBT
});

new Rickshaw.Graph.Axis.Y.Scaled({
  element: document.getElementById('axis1'),
  graph: graph,
  orientation: 'right',
  scale: scales[0],
  // tickFormat: Rickshaw.Fixtures.Number.formatKMBT
});

new Rickshaw.Graph.Axis.Time({
  graph: graph
});


Date.prototype.format = function(format)
{ 
  var o = { 
    "M+" : this.getMonth()+1, //month 
    "d+" : this.getDate(),    //day 
    "h+" : this.getHours(),   //hour 
    "m+" : this.getMinutes(), //minute 
    "s+" : this.getSeconds(), //second 
    "q+" : Math.floor((this.getMonth()+3)/3),  //quarter 
    "S" : this.getMilliseconds() //millisecond 
  } 
  if(/(y+)/.test(format)) format=format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
  for(var k in o)if(new RegExp("("+ k +")").test(format)) 
    format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length)); 
  return format; 
} 

new Rickshaw.Graph.HoverDetail({
  graph: graph,

  formatter: function(series, x, y) {
    var date = '<span class="date">' + new Date( x * 1000 ).format('yyyy/M/d') + '</span>';
    var swatch = '<span class="detail_swatch" style="background-color: ' + series.color + '"></span>';
    var content = date + ' ' + swatch + series.name + ": " + y;
    return content;
  }
});

graph.render();