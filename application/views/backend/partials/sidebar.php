<div class="sidebar clearfix">
  	<ul class="sidebar-panel nav">
	    <li><a href="#"><span class="icon color7"><i class="fa fa-cog"></i></span>系統管理<span class="caret"></span></a>
			<ul style="<?=in_array($this->uri->segments[2], array("cms_user", "cms_system")) ? "display: block;" : ""?>">      	  
				<li><a href="<?=site_url("backend/cms_user/")?>">系統帳號管理</a></li>
				<li><a href="<?=site_url("backend/cms_system/modify/1")?>">系統資訊設定</a></li>
			</ul>
		</li>
	    <li><a href="#"><span class="icon color7"><i class="fa fa-newspaper-o"></i></span>內容管理<span class="caret"></span></a>
			<ul style="<?=in_array($this->uri->segments[2], array("Exchange", "Skill")) ? "display: block;" : ""?>">      	
				<li><a href="<?=site_url("backend/Exchange/Rate")?>">外幣匯率</a></li>  
				<li><a href="<?=site_url("backend/Exchange/")?>">兌匯紀錄</a></li>  
				<li><a href="<?=site_url("backend/Skill/")?>">技能</a></li>  
			</ul>
		</li>
  	</ul>
</div>