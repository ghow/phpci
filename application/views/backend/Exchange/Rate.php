<!DOCTYPE html>
<html lang="en">
<? $this->load->view("backend/partials/meta"); ?>

<body>
  <!-- Start Page Loading -->
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START TOP -->
  <? $this->load->view("backend/partials/top"); ?>
  <!-- END TOP -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEBAR -->
  <? $this->load->view("backend/partials/sidebar"); ?>
  <!-- END SIDEBAR -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START CONTENT -->
  <div class="content">
    <!-- Start Page Header -->
    <div class="page-header">
      <h1 class="title">外幣匯率</h1>
      <ol class="breadcrumb">
        <li><a href="<?=site_url("backend/Exchange/Rate")?>">內容管理</a></li>
        <li class="active">外幣匯率</li>
      </ol>
      <!-- Start Page Header Right Div -->
      <form method="get">
        <div class="right">
          <div class="btn-group" role="group" aria-label="...">
            <a href="javascript:;" class="btn btn-light"><i class="fa fa-calendar"></i></a>
            <input type="month" name="from" id="from" value="<?=$this->input->get('from')?>">
          </div>
        </div>
      </form>
      <!-- End Page Header Right Div -->
    </div>
    <!-- End Page Header -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTAINER -->
    <div class="container-padding">              
      <!-- <? if($msg):?>
        <div class="kode-alert kode-alert-icon kode-alert-click alert3-light">
          <i class="fa fa-check"></i>
          <a href="#" class="closed">&times;</a>
          <?=$msg?>
        </div>
      <? endif;?> -->
      <!-- Start Row -->
      <div class="row">
        <?foreach ($rate as $key => $value):?>
          <!-- Start Panel -->
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-body table-responsive">    
                <!-- Start Chart -->
                <div class="col-lg-12 col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-title">
                      <?=$key?>
                    </div>
                    <div class="panel-body">
                      <div id="rickshaw-<?=$key?>"></div>
                      <input type="hidden" id="<?=$key?>" value=<?=json_encode(array_reverse($value))?>>
                    </div>
                  </div>
                </div>
                <!-- End Chart -->
              </div>
            </div>
          </div>
          <!-- End Panel -->
        <?endforeach?>
      </div>
    </div>
    <!-- END CONTAINER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- Start Footer -->
    <? $this->load->view("backend/partials/footer");?>
    <!-- End Footer -->
  </div>
  <!-- End Content -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEPANEL -->
  <!-- END SIDEPANEL -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <? $this->load->view("backend/partials/script");?>
  <script src="<?=base_url('contents/backend/js/rickshaw/d3.v3.js')?>"></script>
  <script src="<?=base_url('contents/backend/js/rickshaw/rickshaw.js')?>"></script>
  <script src="<?=base_url('contents/backend/js/rickshaw/rickshaw-plugin.js')?>"></script>
  <script>    
    $(document).ready(function() {
      $('#from').change(function() {
        $('form').submit();
      })
    }); 
  </script>
</body>

</html>