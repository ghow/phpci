/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE IF NOT EXISTS `chef` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `chef`;

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
	('065805d4930f5c9d636aaa419f01c0b5', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36', 1529981859, 'a:3:{s:9:"user_data";s:0:"";s:8:"cap_word";s:6:"521706";s:4:"user";O:8:"stdClass":16:{s:2:"id";s:1:"1";s:4:"name";s:9:"管理者";s:7:"account";s:5:"admin";s:14:"password_admin";s:5:"admin";s:5:"email";s:22:"a6011034@hodong.com.tw";s:15:"_wysihtml5_mode";N;s:11:"seo_keyword";N;s:4:"sort";N;s:6:"status";s:1:"1";s:5:"click";s:1:"0";s:10:"created_on";N;s:8:"login_on";N;s:8:"login_ip";N;s:10:"updated_on";s:10:"1500028419";s:17:"updated_user_name";s:5:"admin";s:15:"updated_user_id";s:1:"1";}}'),
	('29fa4bc58d466db3366585321e0a0435', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36', 1529995883, 'a:3:{s:9:"user_data";s:0:"";s:8:"cap_word";s:6:"813886";s:4:"user";O:8:"stdClass":16:{s:2:"id";s:1:"1";s:4:"name";s:9:"管理者";s:7:"account";s:5:"admin";s:14:"password_admin";s:5:"admin";s:5:"email";s:22:"a6011034@hodong.com.tw";s:15:"_wysihtml5_mode";N;s:11:"seo_keyword";N;s:4:"sort";N;s:6:"status";s:1:"1";s:5:"click";s:1:"0";s:10:"created_on";N;s:8:"login_on";N;s:8:"login_ip";N;s:10:"updated_on";s:10:"1500028419";s:17:"updated_user_name";s:5:"admin";s:15:"updated_user_id";s:1:"1";}}'),
	('9963653148929ea1b1190dd603bb890e', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36', 1530071276, 'a:3:{s:9:"user_data";s:0:"";s:8:"cap_word";s:6:"676464";s:4:"user";O:8:"stdClass":16:{s:2:"id";s:1:"1";s:4:"name";s:9:"管理者";s:7:"account";s:5:"admin";s:14:"password_admin";s:5:"admin";s:5:"email";s:22:"a6011034@hodong.com.tw";s:15:"_wysihtml5_mode";N;s:11:"seo_keyword";N;s:4:"sort";N;s:6:"status";s:1:"1";s:5:"click";s:1:"0";s:10:"created_on";N;s:8:"login_on";N;s:8:"login_ip";N;s:10:"updated_on";s:10:"1500028419";s:17:"updated_user_name";s:5:"admin";s:15:"updated_user_id";s:1:"1";}}');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `cms_systems` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT '',
  `site_name` varchar(500) DEFAULT NULL,
  `back_logo` varchar(500) DEFAULT NULL,
  `frond_logo` varchar(500) DEFAULT NULL,
  `mune_lunch` varchar(500) DEFAULT NULL,
  `mune_dinner` varchar(500) DEFAULT NULL,
  `mune_special` varchar(500) DEFAULT NULL,
  `content` text,
  `_wysihtml5_mode` tinyint(4) DEFAULT NULL,
  `seo_keyword` varchar(30) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` tinyint(11) DEFAULT NULL,
  `click` int(11) NOT NULL DEFAULT '0',
  `created_on` int(11) DEFAULT NULL,
  `updated_on` int(11) DEFAULT NULL,
  `updated_user_name` varchar(20) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `cms_systems` DISABLE KEYS */;
INSERT INTO `cms_systems` (`id`, `name`, `site_name`, `back_logo`, `frond_logo`, `mune_lunch`, `mune_dinner`, `mune_special`, `content`, `_wysihtml5_mode`, `seo_keyword`, `sort`, `status`, `click`, `created_on`, `updated_on`, `updated_user_name`, `updated_user_id`) VALUES
	(1, '系統資訊設定', '名廚', '["phone.png"]', '["logo1.png"]', '["pic022.jpg","pic01s.jpg"]', '["pic021.jpg"]', '["img.jpg"]', 'testtest', NULL, NULL, NULL, NULL, 0, NULL, 1489506236, 'admin', 1);
/*!40000 ALTER TABLE `cms_systems` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `cms_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `account` varchar(100) DEFAULT NULL,
  `password_admin` varchar(100) DEFAULT '',
  `email` varchar(500) DEFAULT NULL,
  `_wysihtml5_mode` tinyint(4) DEFAULT NULL,
  `seo_keyword` varchar(30) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` tinyint(11) DEFAULT NULL,
  `click` int(11) NOT NULL DEFAULT '0',
  `created_on` int(11) DEFAULT NULL,
  `login_on` int(11) DEFAULT NULL,
  `login_ip` varchar(50) DEFAULT NULL,
  `updated_on` int(11) DEFAULT NULL,
  `updated_user_name` varchar(20) DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `cms_users` DISABLE KEYS */;
INSERT INTO `cms_users` (`id`, `name`, `account`, `password_admin`, `email`, `_wysihtml5_mode`, `seo_keyword`, `sort`, `status`, `click`, `created_on`, `login_on`, `login_ip`, `updated_on`, `updated_user_name`, `updated_user_id`) VALUES
	(1, '管理者', 'admin', 'admin', 'a6011034@hodong.com.tw', NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, 1500028419, 'admin', 1);
/*!40000 ALTER TABLE `cms_users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
