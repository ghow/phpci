<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Frontend_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{		
		$this->load->view("frontend/index", $this->data);
	}

	public function getToken()
	{
		$this->data["URL"] = "https://notify-bot.line.me/oauth/token";

		$this->data["notify_info"] = array(
			'grant_type'		=>	"authorization_code",
			'code'				=>	"NWNfbnpkt0mal1GxzYbPkr",
			'redirect_uri'		=>	"http://localhost:9090/phpci",
			'client_id'			=>	"9Zp2HDQTlgTTwm6nQD2wRh",
			'client_secret' 	=>	"QW7753akMA7t9v9BDHJ22odKfFb363Bsi5Vdy9ZKjU9",
		);

		$this->load->view("frontend/notify",$this->data);
	}

	private function SendMessage($message)
	{
		//url-ify the data for the POST
		// $fields_string = '';
		// foreach($data as $key=>$value) {
		// 	$fields_string .= $key.'='.$value.'&'; 
		// }
		// $fields_string = rtrim($fields_string,'&');

		// open connection
		$ch = curl_init();
		$URL = "https://notify-api.line.me/api/notify";
		$hearders = array();
		$hearders[] = 'Content-Type: application/x-www-form-urlencoded';
		$hearders[] = 'Authorization: Bearer Z2Z4YbeC3EIELD2cO7qAcy7gGkCqKj8QuaQvkI37kr6';
		$message = "message=".$message ;

		// curl Header
		curl_setopt($ch, CURLOPT_HTTPHEADER, $hearders);

		// curl 資料
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_URL, $URL);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $message);

		// curl 設定
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10); # timeout after 10 seconds, you can increase it
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  # Set curl to return the data instead of printing it to the browser.
	   	curl_setopt($ch,  CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)"); # Some server may refuse your request if you dont pass user agent
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		//execute curl
		$result = curl_exec($ch);
		curl_close($ch);
	}

	public function CheckExchange(){
   		$this->load->library('simple_html_dom');
   		log_message('error', 'CheckExchange');

   		$rate["AUD"] = $this->getRate("AUD");
   		$rate["GBP"] = $this->getRate("GBP");
   		$rate["JPY"] = $this->getRate("JPY");
   		$rate["USD"] = $this->getRate("USD");
   		$rate["CNY"] = $this->getRate("CNY");

   		foreach ($rate as $key => $currency) {
   			$percent = $currency[0]["buy"] / $currency[1]["buy"];
   			if($percent > 1) {
   				$message = $key . "漲 " . number_format(($percent - 1) * 100, 3) . "％";
   			} else {
   				$message = $key . "跌 " . number_format((1 - $percent) * 100, 3) . "％";
   			}

			$this->SendMessage($message);
   		}
	}

	private function getRate($currency)
	{
		$result = array();
		if($this->input->get("from"))
			$get = "?from=" . $this->input->get("from");
		else
			$get = "";

		header("Content-Type:text/html; charset=utf-8");
		$uri = 'http://www.bestxrate.com/bankrate/twesun/' . $currency . '/buytt.html' . $get; 
		$html = file_get_html($uri); 

		$table = $html->find("table");  
		$table = $table[1]->find("tbody tr td");

		switch ($currency) {
			case 'AUD':
				$deviation = 0.2;
				break;
			case 'GBP':
				$deviation = 0.4;
				break;
			case 'JPY':
				$deviation = 0.004;
				break;
			case 'USD':
				$deviation = 0.1;
				break;
			case 'CNY':
				$deviation = 0.05;
				break;
			
			default:
				$deviation = 0;
				break;
		}
		$result = $this->AnalysisRate($table, $deviation);

		return $result;
	}

	private function AnalysisRate($table, $deviation)
	{
		$i = 1;
		$result = array();
		$exchanges = array();

		for ($i=0; $i < count($table); $i += 5) 
		{ 
			$exchanges["date"] = $table[$i]->plaintext;
			$exchanges["buy"] = (float) $table[$i + 2]->plaintext;
			$exchanges["sell"] = (float) $table[$i + 2]->plaintext + $deviation;
			$result[] = $exchanges;
		}
		return $result;
	}
}

// Z2Z4YbeC3EIELD2cO7qAcy7gGkCqKj8QuaQvkI37kr6