<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cms_user_m extends Backend_Model {

    function __construct()
    {
    	$this->validate = 
		array(
			array(
			    'field' => 'account',
			    'label' => '帳號',
			    'rules' => 'required|is_unique[cms_users.account]',
			),
			array(
			    'field' => 'password_admin',
			    'label' => '密碼',
			    'rules' => 'required',
			)
		);
        // Call the Model constructor
        parent::__construct();
    }
    
    public function update($id,$data,$skip_validation)
    {
	    $item = $this->get($id);
	    
	    if($item->account == $data["account"]):
	    	$this->validate = 
			array(
				array(
				    'field' => 'account',
				    'label' => '帳號',
				    'rules' => 'required',
				),
				array(
				    'field' => 'password_admin',
				    'label' => '密碼',
				    'rules' => 'required',
				)
			);
		else:
			array(
				array(
				    'field' => 'account',
				    'label' => '帳號',
				    'rules' => 'required|is_unique[cms_users.account]',
				),
				array(
				    'field' => 'password_admin',
				    'label' => '密碼',
				    'rules' => 'required',
				)
			);
		endif;

	    return parent::update($id,$data,$skip_validation);
    }
}