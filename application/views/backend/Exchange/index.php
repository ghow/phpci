<!DOCTYPE html>
<html lang="en">
<? $this->load->view("backend/partials/meta"); ?>

<body>
  <!-- Start Page Loading -->
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START TOP -->
  <? $this->load->view("backend/partials/top"); ?>
  <!-- END TOP -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEBAR -->
  <? $this->load->view("backend/partials/sidebar"); ?>
  <!-- END SIDEBAR -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START CONTENT -->
  <div class="content">
    <!-- Start Page Header -->
    <div class="page-header">
      <h1 class="title">兌匯記錄</h1>
      <ol class="breadcrumb">
        <li><a href="<?=site_url("backend/Exchange/Rate")?>">內容管理</a></li>
        <li class="active">兌匯記錄列表</li>
      </ol>
      <!-- Start Page Header Right Div -->
      <div class="right">
        <div class="btn-group" role="group" aria-label="...">
          <a href="#" class="btn btn-light"><i class="fa fa-dollar"></i><?=number_format($count)?></a>
          <a href="<?=site_url($site_root."/modify")?>" class="btn btn-light"><i class="fa fa-plus"></i></a>
        </div>
      </div>
      <!-- End Page Header Right Div -->
    </div>
    <!-- End Page Header -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTAINER -->
    <div class="container-padding">              
      <? if($msg):?>
        <div class="kode-alert kode-alert-icon kode-alert-click alert3-light">
          <i class="fa fa-check"></i>
          <a href="#" class="closed">&times;</a>
          <?=$msg?>
        </div>
      <? endif;?>
      <!-- Start Row -->
      <div class="row">
        <!-- Start Panel -->
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-body table-responsive">
              <? if($pagination):?><ul class="pagination"><?=$pagination?></ul><? endif;?>
              <table class="table table-hover" id="dt">
                <thead>
                  <tr>
                    <td style="width: 5%">幣別</td>
                    <td style="width: 5%">買入匯率</td>
                    <td style="width: 5%">賣出匯率</td>
                    <td style="width: 5%">買入金額</td>
                    <td style="width: 5%">買入時間</td>
                    <td style="width: 5%">獲利</td>
                    <td style="width: 5%">狀態</td>
                    <td style="width: 15%">功能</td>
                  </tr>
                </thead>
                <tbody>
                <?foreach($items as $item):?>
                  <tr data="<?=$item->id?>">
                    <td><?=$item->currency?></td>
                    <td><?=$item->rate_buy?></td>
                    <td><?=$item->rate_sell?$item->rate_sell:""?></td>
                    <td><?=number_format($item->amount_buy)?></td>
                    <td><?=date('Y-m-d', $item->time_buy)?></td>
                    <?$amount = $item->amount_sell - $item->amount_buy;?>
                    <td><?=$item->amount_sell > 0? number_format($amount) : "0"?></td>
                    <td class="hidden-xs"><?=$item->status==1?"已賣出":"無"?></td>
                    <td>
                      <a href="<?=site_url($site_root.'/modify/'.$item->id)?>" class="btn btn-light btn-sm">編輯</a>
                      <a href="#" class="btn btn-light btn-sm delete">刪除</a>
                    </td>
                  </tr>
                <?endforeach;?>
                </tbody>
              </table>
            <? if($pagination):?><ul class="pagination"><?=$pagination?></ul><? endif;?>
            </div>
          </div>
        </div>
        <!-- End Panel -->
      </div>
      <!-- End Row -->
    </div>
    <!-- END CONTAINER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- Start Footer -->
    <? $this->load->view("backend/partials/footer");?>
    <!-- End Footer -->
  </div>
  <!-- End Content -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEPANEL -->
  <!-- END SIDEPANEL -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <? $this->load->view("backend/partials/script");?>
  <script>    
    $(document).ready(function() {
      $('#dt').DataTable({
          "paging": false,
          "order": [[4, 'desc']]
      });
    });
  </script>
</body>

</html>