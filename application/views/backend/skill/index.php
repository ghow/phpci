<!DOCTYPE html>
<html lang="en">
<? $this->load->view("backend/partials/meta"); ?>

<body>
  <!-- Start Page Loading -->
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START TOP -->
  <? $this->load->view("backend/partials/top"); ?>
  <!-- END TOP -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEBAR -->
  <? $this->load->view("backend/partials/sidebar"); ?>
  <!-- END SIDEBAR -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START CONTENT -->
  <div class="content">
    <!-- Start Page Header -->
    <div class="page-header">
      <h1 class="title">技能</h1>
      <ol class="breadcrumb">
        <li><a href="<?=site_url("backend/Skill")?>">內容管理</a></li>
        <li class="active">技能列表</li>
      </ol>
      <!-- Start Page Header Right Div -->
      <div class="right">
        <div class="btn-group" role="group" aria-label="...">
          <a href="javascript:;" class="btn btn-light add-button"><i class="fa fa-plus"></i></a>
        </div>
      </div>
      <!-- End Page Header Right Div -->
    </div>
    <!-- End Page Header -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTAINER -->
    <div class="container-padding">     
      <!-- Start Row -->
      <div class="row">
        <!-- Start Panel -->
        <? shuffle($items);
        foreach ($items as $key => $value): ?>
          <div class="col-md-6 col-lg-4">
            <div class="panel panel-default">
              <div class="panel-title">
                <p><?=$value->title?></p>
                <ul class="panel-tools">
                  <li><a class="icon edit-tool"><i class="fa fa-edit"></i></a></li>
                  <li><a class="icon minimise-tool"><i class="fa fa-minus"></i></a></li>
                  <li><a class="icon delete-tool"><i class="fa fa-times"></i></a></li>
                </ul>
              </div>
              <div class="panel-body"><?=nl2br($value->content)?></div>
            </div>

            <form>
              <div class="panel panel-default" hidden>
                <div class="panel-title">
                  <input type="hidden" name="id" value="<?=$value->id?>">
                  <input type="text" name="title" value="<?=$value->title?>">
                  <ul class="panel-tools">
                    <li><a class="icon check-tool"><i class="fa fa-check"></i></a></li>
                    <li><a class="icon cancel-tool"><i class="fa fa-times"></i></a></li>
                  </ul>
                </div>

                <div class="panel-body">
                  <textarea class="form-control" name="content" rows="5" style="resize: none;"><?=$value->content?></textarea>
                </div>
              </div>
            </form>
          </div>
        <? endforeach; ?>

        <div class="col-md-6 col-lg-4">
          <form>
            <div class="panel panel-default" id="new" hidden>
              <div class="panel-title">
                <input type="text" name="title" value="">
                <ul class="panel-tools">
                  <li><a class="icon add-tool"><i class="fa fa-check"></i></a></li>
                  <li><a class="icon cancelAdd-tool"><i class="fa fa-times"></i></a></li>
                </ul>
              </div>

              <div class="panel-body">
                <textarea class="form-control" name="content" rows="5" style="resize: none;"></textarea>
              </div>
            </div>
          </form>
        </div>
        <!-- End Panel -->
      </div>
      <!-- End Row -->
    </div>
    <!-- END CONTAINER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- Start Footer -->
    <? $this->load->view("backend/partials/footer");?>
    <!-- End Footer -->
  </div>
  <!-- End Content -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEPANEL -->
  <!-- END SIDEPANEL -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <? $this->load->view("backend/partials/script");?>
  <script>  
  $().ready(function(){
    $(".edit-tool").click(function(){
      $(this).parents().eq(3).hide();
      $(this).parents().eq(3).next().children().show();
    });

    $(".check-tool").click(function(){
      var form = $(this).parents().eq(4);

      url = "<?=base_url("backend/skill/ajaxModify")?>";
      $.ajax({
        url: url,
        type: "POST",
        data: form.serialize(),
        dataType: "json",
        error: function(err){
          console.log(err);
        },
        success: function(result){
          if(result.type == "error")
            alert(result.message);
          else if(result.type == "success")
          {
            // 隱藏方塊
            form.children().hide();
            form.prev().show();

            // 替換內容
            form.prev().children().eq(0).find("p").text(form.find("input[name='title']").val());
            form.prev().children().eq(1).html(result.message);
          }
        }
      });
    });

    $(".delete-tool").click(function(){
      var id = $(this).parents().eq(4).find("input[name='id']").val();
      var div = $(this).parents().eq(4);

      swal({
        title: "確認要刪除嗎?", 
        type: "warning", 
        showCancelButton: true, 
        confirmButtonColor: "#DD6B55", 
        confirmButtonText: "確認", 
        cancelButtonText: "取消",
      },
      function(){
        ajaxDelete(id);
      });
    });

    $(".add-tool").click(function(){
      var form = $(this).parents().eq(4);

      url = "<?=base_url("backend/skill/ajaxAdd")?>";
      $.ajax({
        url: url,
        type: "POST",
        data: form.serialize(),
        dataType: "json",
        error: function(err){
          console.log(err);
        },
        success: function(result){
          if(result.type == "error")
            alert(result.message);
          else if(result.type == "success")
            location.reload();
        }
      });
    });

    $(".cancel-tool").click(function(){
      $(this).parents().eq(3).hide();
      $(this).parents().eq(4).prev().show();
    });

    $(".add-button").click(function(){
      $("#new").show();
    });

    $(".cancelAdd-tool").click(function(){
      $("#new").fadeOut();
    });

    $("input").keypress(function (event) {
      if (event.which == 13) 
        return false;
    });
  }); 

  function ajaxDelete(id){
    url = "<?=base_url("backend/skill/ajaxDelete")?>";

    $.ajax({
      url: url,
      type: "POST",
      data: { id: id },
      dataType: "json",
      error: function(err){
        console.log(err);
      },
      success: function(result){
        if(result.type == "error")
          alert(result.message);
        else if(result.type == "success") 
          $("input[value='" + id + "']").parents().eq(3).fadeOut();
      }
    });
  }

  </script>
</body>

</html>