<?php 

class Action extends Backend_Controller 
{
	public function __construct()
	{
		parent::__construct("backend");			

		$this->per_page = 100;
	}

	/* EXCEL匯入 開始 */
	public function import_xls()
	{
		$this->load->helper(array('form','url'));
		
		$config['upload_path'] = './upload/import_xls/';
		$config['allowed_types'] = 'xls|xlsx';
		$config['max_size']	= '10240';
		$config['overwrite'] = 'TRUE';

		$this->load->library('upload',$config);
		
		$filed_name = "import_xls";
		
		if (!$this->upload->do_upload($filed_name))
		{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata('err_msg', "上傳失敗:".$error);
			redirect(site_url("backend/member"));
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$this->Import($data["upload_data"]["orig_name"]);
		}
		
	}
	
	function import($upload_file)
    {
		$this->load->library('Excel_Factory');
		
		$objPHPExcel = $this->excel_factory->load("./upload/import_xls/$upload_file");
		
		$objPHPExcel->setActiveSheetIndex(0);
		$sheet = $objPHPExcel->getActiveSheet();
		$highestRow = $sheet->getHighestRow(); // 取得總行數
		$ColumnName = $this->getColumnName();
		$err_msg = "";
		$this->in = 0;
		$this->up = 0;
		
		// 欄位驗證
		if(	$objPHPExcel->getActiveSheet()->getCell("A1")->getvalue() == "姓名" &&
			$objPHPExcel->getActiveSheet()->getCell("C1")->getvalue() == "email" &&
			$objPHPExcel->getActiveSheet()->getCell("D1")->getvalue() == "手機" &&
			$objPHPExcel->getActiveSheet()->getCell("E1")->getvalue() == "電話" &&
			$objPHPExcel->getActiveSheet()->getCell("F1")->getvalue() == "密碼")
		{		
			//循環讀取excel文件,讀取一條,插入一條
			for($j=2;$j<=$highestRow;$j++)
			{
				$data = array();
				foreach($ColumnName as $k=>$v)
					$data[$v] = $objPHPExcel->getActiveSheet()->getCell("$k$j")->getValue();
				$resoult = $this->sync_data($data);

				if($resoult === "email")
					$err_msg .= "第".$j."行 email格式錯誤</br>";
				if($resoult === "insert")
					$err_msg .= "第".$j."行 email或phone重複</br>";
			}
		}else
			$this->session->set_flashdata('err_msg', "檔案格式錯誤");
		
		$sync_result = "上傳成功，新增：".$this->in." 筆";
		$this->session->set_flashdata('err_msg', $err_msg);
		$this->session->set_flashdata('msg', $sync_result);
		redirect(site_url("backend/member"));
		
	}
	
	private function sync_data($data)
	{		
		if(!$this->model->insert($data))
		{
			return "insert";
		}
		$this->in++;
		return TRUE;			
	}

	// 對應欄位
	private function getColumnName()
	{
		$ColumnName = array(
			"A"	=>	"name",
			"B"	=>	"department",
			"C"	=>	"email",
			"D"	=>	"phone",
			"E"	=>	"tel",
			"F"	=>	"password",
			"G"	=>	"level",
		);		
		return $ColumnName;
	}
	/* EXCEL匯入 結束 */

	/* EXCEL匯出 開始 */
	public function export($status)
	{		
	    $th["A1"] = "姓名";
	    $th["B1"] = "Email";
	    $th["C1"] = "手機";
	    $th["D1"] = "電話";
	    $th["E1"] = "企業 / 單位";
	    $th["F1"] = "會員等級";
	    $th["G1"] = "建立時間";

	    $where["status"] = $status;
	    $result = $this->model->get_many_by($where);
	    
	    $num = 2;
	    foreach ($result as $v) 
	    {
	    	$th["A".$num] = $v->name;
	    	$th["B".$num] = $v->email;
	    	$th["C".$num] = $v->phone;
	    	$th["D".$num] = $v->tel;
	    	$th["E".$num] = $v->department;
	    	$th["F".$num] = $this->getLevelName($v->level);
	    	$th["G".$num] = date('Y-m-d H:i:s', local_to_gmt($v->created_on));
	    	$num++;
	    }
	    
	   	//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('標題');
		//Set column widths
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(16);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(64);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(16);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(16);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(16);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(16);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(32);
		
		foreach($th as $k=>$v)
		{
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue($k, $v);
			//change the font size
			$this->excel->getActiveSheet()->getStyle($k)->getFont()->setSize(12);
			//set aligment to center for that merged cell (A1 to D1)
			$this->excel->getActiveSheet()->getStyle($k)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}
		
		$filename='標題'.date('YmdHis').'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		             
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		@$objWriter->save('php://output');
		exit();
	}
	/* EXCEL匯出 結束 */

	/* AJAX submit 開始 */
	public function ajax_submit()
	{	
		$result["type"] = "error";
		$result["message"] = "錯誤";
		echo json_encode($result);
	}
	/* AJAX submit 結束 */
}