<!DOCTYPE html>
<html lang="en">
<? $this->load->view("backend/partials/meta"); ?>

<body>
  <!-- Start Page Loading -->
  <!-- End Page Loading -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START TOP -->
  <? $this->load->view("backend/partials/top"); ?>
  <!-- END TOP -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEBAR -->
  <? $this->load->view("backend/partials/sidebar"); ?>
  <!-- END SIDEBAR -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START CONTENT -->
  <div class="content">
    <!-- Start Page Header -->
    <div class="page-header">
      <h1 class="title">兌匯記錄</h1>
      <ol class="breadcrumb">
        <li><a href="<?=site_url("backend/Exchange/Rate")?>">內容管理</a></li>
        <li><a href="<?=site_url("backend/Exchange")?>">兌匯記錄列表</a></li>
        <li class="active">編輯</li>
      </ol>
      <!-- Start Page Header Right Div -->
      <!-- End Page Header Right Div -->
    </div>
    <!-- End Page Header -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTAINER -->
    <div class="container-padding">
      <? if($err_msg):?>
      <? foreach($err_msg as $k=>$v):?>
        <div class="kode-alert kode-alert-icon kode-alert-click alert6-light">
          <i class="fa fa-warning"></i>
          <a href="#" class="closed">&times;</a>
          <?=$v?>
        </div>
      <? endforeach;?>
      <? endif;?>
      <!-- Start Row -->
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-title">
            </div>
            <div class="panel-body">
              <form class="form-horizontal" method="post">
                <div class="form-group">
                  <div class="col-sm-6"> 
                    <label class="col-sm-2 control-label form-label"><span style="color:red">  </span>幣別</label>
                    <div class="col-sm-10">
                      <select name="currency">
                        <option value="美金" <?=$item ? $item->currency == "美金" ? "selected" : "" : ""?>>美金</option>
                        <option value="人民幣" <?=$item ? $item->currency == "人民幣" ? "selected" : "" : ""?>>人民幣</option>
                        <option value="澳幣" <?=$item ? $item->currency == "澳幣" ? "selected" : "" : ""?>>澳幣</option>
                        <option value="英鎊" <?=$item ? $item->currency == "英鎊" ? "selected" : "" : ""?>>英鎊</option>
                        <option value="日幣" <?=$item ? $item->currency == "日幣" ? "selected" : "" : ""?>>日幣</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-6"> 
                    <label class="col-sm-2 control-label form-label"><span style="color:red">  </span>買入匯率</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="rate_buy" value="<?=$item?$item->rate_buy:""?>">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-6"> 
                    <label class="col-sm-2 control-label form-label">買入金額</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="amount_buy" value="<?=$item?$item->amount_buy:""?>">
                    </div>
                  </div>
                  <div class="col-sm-6"> 
                    <label class="col-sm-2 control-label form-label">買入日期</label>
                    <div class="col-sm-10">
                      <input type="date" class="form-control" name="time_buy" value="<?=$item?date('Y-m-d', $item->time_buy):date('Y-m-d')?>">
                    </div>
                  </div>
                </div>
                <hr>

                <div class="form-group">
                  <div class="col-sm-6"> 
                    <label class="col-sm-2 control-label form-label">賣出金額</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="amount_sell" value="<?=$item?$item->amount_sell:""?>">
                    </div>
                  </div>
                  <div class="col-sm-6"> 
                    <label class="col-sm-2 control-label form-label">賣出匯率</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="rate_sell" value="<?=$item?$item->rate_sell:""?>">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-6"> 
                    <label class="col-sm-2 control-label form-label"><span style="color:red"></span>賣出</label>
                    <div class="col-sm-10">
                      <input type="checkbox" <?=$item?$item->status?"checked":"":""?> data-toggle="toggle" data-onstyle="success" name="status" value="1">
                    </div>
                  </div>
                  <div class="col-sm-6"> 
                    <label class="col-sm-2 control-label form-label">賣出日期</label>
                    <div class="col-sm-10">
                      <input type="date" class="form-control" name="time_sell" value="<?=$item?date('Y-m-d', $item->time_sell):date('Y-m-d')?>">
                    </div>
                  </div>
                </div>
                <p class="col-sm-offset-2"><span style="color:red"></span></p>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default submit">送出</button>
                    <button class="btn btn-light" type="button" onclick="location.href='<?=site_url($site_root)?>'">返回</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row -->
    </div>
    <!-- END CONTAINER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- Start Footer -->
    <? $this->load->view("backend/partials/footer");?>
    <!-- End Footer -->
  </div>
  <!-- End Content -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
  <!-- START SIDEPANEL -->
  <!-- END SIDEPANEL -->
  <!-- //////////////////////////////////////////////////////////////////////////// -->
<? $this->load->view("backend/partials/script");?>
<script>
</script>
</body>
</html>