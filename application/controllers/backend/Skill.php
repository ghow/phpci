<?php 

class Skill extends Backend_Controller 
{
	public function __construct()
	{
		parent::__construct("backend");			

		$this->per_page = 100;
	}

	public function ajaxModify()
	{	
		if($this->input->post("title") != "")
		{
			$this->model->update($this->input->post("id"), $this->input->post());
			$result["type"] = "success";
			$result["message"] = nl2br($this->input->post("content"));
		} else {
			$result["type"] = "error";
			$result["message"] = "標題不得空白!";
		}

		echo json_encode($result);
	}

	public function ajaxAdd()
	{	
		if($this->input->post("title") != "")
		{
			$this->model->insert($this->input->post());
			$result["type"] = "success";
		} else {
			$result["type"] = "error";
			$result["message"] = "標題不得空白!";
		}

		echo json_encode($result);
	}

	public function ajaxDelete()
	{	
		if($this->input->post())
		{
			$this->model->delete($this->input->post("id"));
			$result["type"] = "success";
		} else {
			$result["type"] = "error";
			$result["message"] = "標題不得空白!";
		}

		echo json_encode($result);
	}

	public function index($page = 0)
	{
		parent::index($page);
	}
}