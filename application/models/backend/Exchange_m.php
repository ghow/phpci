<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Exchange_m extends Backend_Model {

    function __construct()
    {
    	$this->validate = array();
        // Call the Model constructor
        parent::__construct();

        $this->order = "id desc";
    }
}